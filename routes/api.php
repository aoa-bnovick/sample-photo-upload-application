<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Gets list of programs for all forms
Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::middleware('ldapusercheck')->post('upload', ['uses' => 'AOAStaffPicController@upload']);
Route::middleware('ldapusercheck')->post('getStaffPhoto', ['uses' => 'AOAStaffPicController@getStaffPhoto']);
Route::get('getFormattedName', 'Auth\AOALdapController@getFormattedName')->name('getname');
Route::post('aoaldaplogin', 'Auth\AOALdapController@AOALdapLogin')->name('apilogin');
