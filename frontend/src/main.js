/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App'
import router from './router'
import api from './api'
import store from './store'
import 'canvas-toBlob';

Vue.prototype.$http = api
Vue.config.devtools = true
new Vue({
  el: '#vapp',
  router,
  store,
  render: h => h(App)
})
