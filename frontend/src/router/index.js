import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import store from '../store'
import AOAStaffPics from '@/components/AOAStaffPics'
import Unauthorized from '@/components/Unauthorized'
import Login from '../components/Login.vue'

const router = new VueRouter({
    mode: "history",
    base: "/aoastaffphotos",
    routes: [
        {
            path: '/',
            name: 'AOAStaffPics',
            component: AOAStaffPics,
            meta: {
                auth: true
            }
        }, {
            path: '/unauthorized',
            name: 'Unauthorized',
            component: Unauthorized,
            meta: {
                auth: true
            }
        }, {
            path: '/login',
            name: 'Login',
            component: Login,
            meta: {
                auth: false
            }
        }, {
            path: '*',
            redirect: '/'
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (!store.getters.isLogged && to.meta.auth && to.name != 'Login') {
        return next('login')
    }
    if (store.getters.isLogged && to.name === 'Login') {
        return next('/')
    }

    if (store.getters.isLogged && store.getters.isStaff && to.name === 'Unauthorized') {
        return next('/')
    }
    next()

})

export default router