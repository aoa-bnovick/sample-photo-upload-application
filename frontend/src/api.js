/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Axios from 'axios'
import store from './store'
import {AUTH_LOGOUT} from './store/actions/auth.js'

let base = process.env.VUE_APP_APIURL + '/aoastaffphotos';
let api = Axios.create({baseURL: base})

export default api