/* eslint-disable promise/param-names */
import {AUTH_ERROR, AUTH_SUCCESS, AUTH_VALIDATE, AUTH_USER, AUTH_LOGOUT, AUTH_LOGINFAIL, AUTH_LOADING} from '../actions/auth'
import api from '../../api'

const state = {
  token: localStorage.getItem('jwt') || '',
  status: '',
  hasLoadedOnce: false,
  name: ''
}

const getters = {
  isLogged: state => !!state.token,
  isStaff: state => state.mtype == "STAFF",
  getAOAID: state => state.aoaid,
  isError: state => state.status == "error",
  isLoginFail: state => state.status == "loginfail",
}

const actions = {
  [AUTH_VALIDATE]: ({
    commit,
    dispatch
  }, creds) => {
    commit(AUTH_LOADING)
    return new Promise((resolve, reject) => {
      api({
        url: '/api/aoaldaplogin',
        data: {
          username: creds[0],
          password: creds[1]
        },
        method: 'POST'
      }).then(resp => {
        localStorage.setItem('jwt', resp.data.access_token)
        console.log(resp.data.access_token)
        commit(AUTH_SUCCESS, resp.data)
        resolve(resp)
      }).catch(err => {
        commit(AUTH_ERROR, err)
       if(err.response.data.page){
         if(err.response.data.page == "Login" && err.response.data.msg == "Invalid Credentials"){
            commit(AUTH_LOGINFAIL, err)
         }
       }
       reject(err)
      })
    })
  }
}

const mutations = {
  [AUTH_SUCCESS]: (state, data) => {
    state.status = 'success'
    state.token = data.access_token
    state.name = data.name
    state.hasLoadedOnce = true
  },
  [AUTH_ERROR]: (state) => {
    state.status = 'error'
    state.hasLoadedOnce = true
  },
  [AUTH_LOGINFAIL]: (state) => {
    state.status = 'loginfail'
    state.hasLoadedOnce = true
  },
  [AUTH_VALIDATE]: (state) => {
    state.status = 'loading'
  },
  [AUTH_LOADING]: (state)=>{
    state.status = 'loading'
  },
  [AUTH_USER]: (state, name)=>{
    state.name = name
  }
  
}

export default {
  state,
  getters,
  actions,
  mutations
}
