<?php

namespace App\Logging;

use Monolog\Logger;

class AOAITErrorDB
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $handler = new SqlHandler($config,Logger::WARNING);
        return new Logger('custom',[$handler]);
    }
}