<?php

namespace App\Logging;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;
use DB, Exception;

class SqlHandler extends AbstractProcessingHandler
{
    protected $table;
    protected $connection;

    public function __construct(array $config,int $level = Logger::DEBUG,bool $bubble = true) {

        $this->table        = $config['table'];
        $this->connection   = $config['connection'];
        $this->sitename     = $config['sitename'];
        parent::__construct($level, $bubble);
    }

    protected function write(array $record) {
        try {
            $testconnect=DB::connection($this->connection);
            $data = [
                'sitename' => $this->sitename,
                'message' => $record['message'],
                'error_level' => $record['level_name'],
                'remote_addr' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null,
                'user_agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
                'server' => gethostname(),
                'created_at' => $record['datetime']->format('Y-m-d H:i:s')
            ];
            DB::connection($this->connection)->table($this->table)->insert($data);
        } catch (Exception $e) {
            throw new Exception('AOAITError_Logging_Connection'.$e);
        }
        
    }
}