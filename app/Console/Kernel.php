<?php

namespace App\Console;

use DB;
use Log;
use Storage;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $chiou = 'OU=Chicago Staff, DC=aoanet,DC=local';
            $chistaff = \Adldap::search()->users()->select(['samaccountname'])->setDn($chiou)->get()->toArray();
            
            $dcou = 'OU=Washington Staff, DC=aoanet,DC=local';
            $dcstaff = \Adldap::search()->users()->select(['samaccountname'])->setDn($dcou)->get()->toArray();
            $staff = array_merge($chistaff, $dcstaff);

            
            $changed = 0;
            foreach ($staff as $user) {
                $filename = $user['samaccountname'][0].".jpg";
                Log::info($filename);
               
                $exists = Storage::disk('local')->exists($filename);
                if (!$exists) {
                    $img= Storage::copy("default.jpg", $filename);
                    $changed ++;
                }
            }
            echo $changed;
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
