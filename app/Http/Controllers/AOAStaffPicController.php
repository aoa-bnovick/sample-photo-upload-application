<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\JsonResponse;
use Carbon\Carbon;
use Exception;
use Log;
use Notification;
use Imagick;

//use App\Notifications\DocumentChanged;

//use AOA\AOAAuth\Models\AOAUser;

class AOAStaffPicController extends Controller
{
    
    /**
     * Helper to begin file upload via Post request
     *
     * @param  Request  $request
     * @return Response
     */
    public function upload(Request $request)
    {
        try {
            if (!$request->usernamefiltered) {
                throw new Exception("User not found");
            }

            $username = $request->usernamefiltered;
            
            if ($request->input('croppedImage') == "default") {
                $default = storage_path('app').'/default.jpg';
                $filename = $username.".jpg";
                if (Storage::exists($filename)) {
                    Storage::delete($filename);
                }
                $img= Storage::copy("default.jpg", $filename);
            } else {
                $uploadedFile = $request->file('croppedImage');
                
                if ($uploadedFile->getClientSize() == 0) {
                    throw new Exception('Something went wrong');
                }

                $filename = $username.".jpg";
                $stored = storage_path('app').'/'.$filename;
                $img = \Image::make($uploadedFile)->resize(150, 150)->encode('jpg')->save($stored);
            }
            if ($img) {
                return response()->json([
                'success' => true,
            ], 200);
            } else {
                throw new Exception('Something went wrong');
            }
        } catch (Exception $e) {
            report($e);
            return response()->json($e->getMessage(), 500);
        }
    }


    public function download(Request $request)
    {
        $requestedFile = $request->input('file');
        // list all filenames in given path
        $exists = Storage::disk('local')->exists($requestedFile);

        if ($exists) {
            return  Storage::disk('local')->get($requestedFile);
        } else {
            return response()->json('Something went wrong', 500);
        }
    }

    public function getStaffPhoto(Request $request)
    {
        if (!$request->usernamefiltered) {
            throw new Exception("User not found");
        }

        $username = $request->usernamefiltered;
        $filename = $username.".jpg";
        
        // list all filenames in given path
        $file = Storage::disk('local')->get("default.jpg");
        $exists = Storage::disk('local')->exists($filename);
        if ($exists) {
            // Compare file with default file
            $defaultdata = \Image::make(storage_path('app').'/default.jpg')->getCore();
            $filedata = \Image::make(storage_path('app').'/'.$filename)->getCore();
            $diffScore=$defaultdata->compareImages($filedata, Imagick::METRIC_MEANSQUAREERROR);
            $isDefault = ($diffScore[1] == 0);
            
            if (!$isDefault) {
                $file = Storage::disk('local')->get($filename);
                return response($file);
            } else {
                return response('Default', 206)
                  ->header('Content-Type', 'appliation/json');
            }
        } else {
            return response('None found', 206)
                  ->header('Content-Type', 'appliation/json');
        }
    }
}
