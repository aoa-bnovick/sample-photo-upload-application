<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\JsonResponse;
use Log;

class AOALdapController extends Controller
{

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function AOALdapLogin(Request $request)
    {
        $un = $request->username;
        $pw = $request->password;

        
        $login =Auth::attempt(array('samaccountname'=>$un, 'password'=>$pw,));
        if ($login == false) {
            return response()->json([
                'success' => false,
                'page' => 'Login',
                'msg' => 'Invalid Credentials'
            ], 401);
        }
        
        $factory = \JWTFactory::customClaims([
            'sub'   => $un
        ]);

        
        $payload = $factory->make();
        $token = \JWTAuth::encode($payload);
        // $jwt = Auth::guard('api')->login($user);
        return $this->respondWithJWTToken($token);
    }

    

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithJWTToken($token)
    {
        return response()->json([
            'access_token' => (string)$token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60,
        ]);
    }

    public function getFormattedName($un = "")
    {
        if ($un =="") {
            $token = \JWTAuth::getToken();

            $apy = \JWTAuth::getPayload($token)->toArray();

            $un = $apy['sub'];
        }

        $search = \Adldap::search()->select(['cn'])->where('samaccountname', '=', $un)->limit(1)->get();
        if ($search[0]['cn']) {
            $formattedname = $search[0]['cn'][0];
        }
   
        return response()->json([
            'name' =>  $formattedname
        ]);
    }
}
