<?php

namespace App\Http\Middleware;

use Closure;

class LDAPUserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->expectsJson()) {
            $unauthorized=false;

            $token = \JWTAuth::getToken();

            if(!$token){
                $unauthorized = true;
            }

            $apy = \JWTAuth::getPayload($token)->toArray();

            if(!array_key_exists('sub', $apy)){
                $unauthorized = true;
            }
            else{
                $username = $apy['sub'];
                $search = \Adldap::search()->where('samaccountname', '=', $username)->get();
                if(!$search){
                    $unauthorized = true;
                }
                $request->usernamefiltered = $username;
            }
        }
        if($unauthorized){
            $response = response()->json(['message' => "unauthorized", 401]);
            return $response;
        }
       

        return $next($request);
    }
}
