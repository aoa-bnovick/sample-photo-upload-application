## Sample Laravel / Vue.js Application - Active Directory Photo Upload

Some features demonstrated in this repo:

- Utilized Adldap2 to authenticate via Active Directory
- API-based, backend is by-and-large separated from frontend. 
- Token-based authentication
- Image analysis and comparison
